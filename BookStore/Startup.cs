﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using BookStore.Models;
using Microsoft.AspNet.OData.Batch;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNet.OData.Routing.Conventions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OData;
using Microsoft.OData.Edm;
using Microsoft.OData.UriParser;

namespace BookStore
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore();
            // services.AddApiVersioning();
            services.AddOData();
            // .EnableApiVersioning();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            var modelBuilder = new ODataConventionModelBuilder();
            var modelConfigurations = Assembly
                .GetAssembly(typeof(Startup))
                .GetTypes()
                .Where(w => typeof(IModelConfiguration).IsAssignableFrom(w))
                .ToList();
            foreach (var modelConfiguration in modelConfigurations)
            {
                var instance = (IModelConfiguration)Activator.CreateInstance(modelConfiguration);
                instance.Apply(modelBuilder, ApiVersion.Default);
            }
            // var modelConfigurations = new IModelConfiguration[] { new ModelConfigurationAccountAddress(), new ModelConfigurationSite() };
            // var apiVersion = new ApiVersion(1, 0);
            // foreach (var modelConfiguration in modelConfigurations)
            // {
            //     modelConfiguration.Apply(modelBuilder, apiVersion);
            // }



            // app.UseMvc(routes => routes.MapVersionedODataRoute("odata", "v{version:apiVersion}", modelBuilder.GetEdmModel(), apiVersion));

            // app.UseMvc(options =>
            // {

            //     var model = modelBuilder.GetEdmModel();
            //     options.MapVersionedODataRoute("odata", "odata", apiVersion, b => b
            //             .AddService(Microsoft.OData.ServiceLifetime.Scoped, s => model)
            //             .AddService<IEnumerable<IODataRoutingConvention>>(
            //                 Microsoft.OData.ServiceLifetime.Scoped,
            //                 s => ODataRoutingConventions.CreateDefaultWithAttributeRouting("odata", options)));
            // });
            app.UseMvc(options =>
            {

                var model = modelBuilder.GetEdmModel();
                options.Select().Expand().Filter().OrderBy().MaxTop(null).Count();
                options.MapRoute("services", "{controller=Unbound}/{action=Index}/{id?}");
                options
                .MapODataServiceRoute(
                    "odata",
                    "odata",
                    b => b
                        .AddService(Microsoft.OData.ServiceLifetime.Scoped, s => model)
                        .AddService<IEnumerable<IODataRoutingConvention>>(
                            Microsoft.OData.ServiceLifetime.Scoped,
                            s => ODataRoutingConventions.CreateDefaultWithAttributeRouting("odata", options))
                        .AddService<IODataPathHandler>(
                            Microsoft.OData.ServiceLifetime.Scoped,
                            s => new StringParameterHandler())
                        .AddService<ODataUriResolver>(
                            Microsoft.OData.ServiceLifetime.Scoped,
                            s => new AlternateKeysResolver(model))
                        .AddService<ODataBatchHandler>(
                            Microsoft.OData.ServiceLifetime.Scoped,
                            s => new DefaultODataBatchHandler())
                            );
            });
        }
    }

    /// <summary>
    /// Defines the configuration of the <see cref="AccountAddressesController" />.
    /// </summary>
    public class ModelConfigurationAccountAddress : IModelConfiguration
    {
        /// <summary>
        /// Applies the configuration for the given <see cref="ApiVersion" />.
        /// </summary>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            switch (apiVersion.MajorVersion)
            {
                case 1:
                    ConfigureV1(builder);
                    break;
                default:
                    ConfigureV1(builder);
                    break;
            }
        }

        /// <summary>
        /// Applies the configuration for version 1 of the controller.
        /// </summary>
        private void ConfigureV1(ODataModelBuilder builder)
        {
            builder.EntitySet<Account>("Accounts");
            builder.EntityType<PaymentInstrument>();
        }
    }

    public class ModelConfigurationSite : IModelConfiguration
    {
        /// <summary>
        /// Applies the configuration for the given <see cref="ApiVersion" />.
        /// </summary>
        public void Apply(ODataModelBuilder builder, ApiVersion apiVersion)
        {
            switch (apiVersion.MajorVersion)
            {
                case 1:
                    ConfigureV1(builder);
                    break;
                default:
                    ConfigureV1(builder);
                    break;
            }
        }

        /// <summary>
        /// Applies the configuration for version 1 of the controller.
        /// </summary>
        private void ConfigureV1(ODataModelBuilder builder)
        {
            builder.EntitySet<Site>("Sites");
            // builder.EntityType<SiteFunction>();

            builder.EntitySet<SiteFunction>("SiteFunctions");
            builder.EntityType<Site>().ContainsMany(e => e.SiteFunctions);

        }
    }


    /// <summary>
    /// Implementation for resolving the alternate keys.
    /// </summary>
    public class AlternateKeysResolver : UnqualifiedODataUriResolver
    {
        /// <summary>
        /// Model to be used for resolving the alternate keys.
        /// </summary>
        private readonly IEdmModel model;

        /// <summary>
        /// Constructs a AlternateKeysODataUriResolver with the given edmModel to be used for resolving alternate keys
        /// </summary>
        /// <param name="model">The model to be used.</param>
        public AlternateKeysResolver(IEdmModel model)
        {
            this.model = model;
        }

        ///// <summary>
        ///// Resolve unbound operations based on name.
        ///// </summary>
        ///// <param name="model">The model to be used.</param>
        ///// <param name="identifier">The operation name.</param>
        ///// <returns>Resolved operation list.</returns>
        //public override IEnumerable<IEdmOperation> ResolveUnboundOperations(IEdmModel model, string identifier)
        //{
        //    if (identifier.Contains("."))
        //    {
        //        return base.ResolveUnboundOperations(model, identifier);
        //    }

        //    return FindAcrossModels<IEdmOperation>(model, identifier, this.EnableCaseInsensitive)
        //            .Where(operation => !operation.IsBound);
        //}

        ///// <summary>
        ///// Resolve bound operations based on name.
        ///// </summary>
        ///// <param name="model">The model to be used.</param>
        ///// <param name="identifier">The operation name.</param>
        ///// <param name="bindingType">The type operation was binding to.</param>
        ///// <returns>Resolved operation list.</returns>
        //public override IEnumerable<IEdmOperation> ResolveBoundOperations(IEdmModel model, string identifier, IEdmType bindingType)
        //{
        //    if (identifier.Contains("."))
        //    {
        //        return base.ResolveBoundOperations(model, identifier, bindingType);
        //    }

        //    return FindAcrossModels<IEdmOperation>(model, identifier, this.EnableCaseInsensitive)
        //        .Where(operation =>
        //            operation.IsBound
        //            && operation.Parameters.Any()
        //            && operation.HasEquivalentBindingType(bindingType));
        //}

        //private static IEnumerable<T> FindAcrossModels<T>(IEdmModel model, String qualifiedName, bool caseInsensitive) where T : IEdmSchemaElement
        //{
        //    Func<IEdmModel, IEnumerable<T>> finder = (refModel) =>
        //        refModel.SchemaElements.OfType<T>()
        //        .Where(e => string.Equals(qualifiedName, e.Name, caseInsensitive ? StringComparison.OrdinalIgnoreCase : StringComparison.Ordinal));

        //    IEnumerable<T> results = finder(model);

        //    foreach (IEdmModel reference in model.ReferencedModels)
        //    {
        //        results.Concat(finder(reference));
        //    }

        //    return results;
        //}

        /// <summary>
        /// Resolve keys for certain entity set, this function would be called when key is specified as name value pairs. E.g. EntitySet(ID='key')
        /// </summary>
        /// <param name="type">Type for current entityset.</param>
        /// <param name="namedValues">The dictionary of name value pairs.</param>
        /// <param name="convertFunc">The convert function to be used for value converting.</param>
        /// <returns>The resolved key list.</returns>
        public override IEnumerable<KeyValuePair<string, object>> ResolveKeys(IEdmEntityType type, IDictionary<string, string> namedValues, Func<IEdmTypeReference, string, object> convertFunc)
        {
            IEnumerable<KeyValuePair<string, object>> convertedPairs;
            try
            {
                convertedPairs = base.ResolveKeys(type, namedValues, convertFunc);
            }
            catch (ODataException)
            {
                if (!TryResolveAlternateKeys(type, namedValues, convertFunc, out convertedPairs))
                {
                    throw;
                }
            }

            return convertedPairs;
        }

        /// <summary>
        /// Try to resolve alternate keys for certain entity type, this function would be called when key is specified as name value pairs. E.g. EntitySet(ID='key')
        /// </summary>
        /// <param name="type">Type for current entityset.</param>
        /// <param name="namedValues">The dictionary of name value pairs.</param>
        /// <param name="convertFunc">The convert function to be used for value converting.</param>
        /// <param name="convertedPairs">The resolved key list.</param>
        /// <returns>True if resolve succeeded.</returns>
        private bool TryResolveAlternateKeys(IEdmEntityType type, IDictionary<string, string> namedValues, Func<IEdmTypeReference, string, object> convertFunc, out IEnumerable<KeyValuePair<string, object>> convertedPairs)
        {
            IEnumerable<IDictionary<string, IEdmProperty>> alternateKeys = model.GetAlternateKeysAnnotation(type);
            foreach (IDictionary<string, IEdmProperty> keys in alternateKeys)
            {
                if (TryResolveKeys(type, namedValues, keys, convertFunc, out convertedPairs))
                {
                    return true;
                }
            }

            convertedPairs = null;
            return false;
        }

        /// <summary>
        /// Try to resolve keys for certain entity type, this function would be called when key is specified as name value pairs. E.g. EntitySet(ID='key')
        /// </summary>
        /// <param name="type">Type for current entityset.</param>
        /// <param name="namedValues">The dictionary of name value pairs.</param>
        /// <param name="keyProperties">Dictionary of alias to key properties.</param>
        /// <param name="convertFunc">The convert function to be used for value converting.</param>
        /// <param name="convertedPairs">The resolved key list.</param>
        /// <returns>True if resolve succeeded.</returns>
        private bool TryResolveKeys(IEdmEntityType type, IDictionary<string, string> namedValues, IDictionary<string, IEdmProperty> keyProperties, Func<IEdmTypeReference, string, object> convertFunc, out IEnumerable<KeyValuePair<string, object>> convertedPairs)
        {
            if (namedValues.Count != keyProperties.Count)
            {
                // Count of name value pair does not match the alias count in this set of
                // alternative keys ==> Unresolvable for this set.
                convertedPairs = null;
                return false;
            }

            Dictionary<string, object> pairs = new Dictionary<string, object>(StringComparer.Ordinal);

            foreach (KeyValuePair<string, IEdmProperty> kvp in keyProperties)
            {
                string valueText;

                if (!namedValues.TryGetValue(kvp.Key, out valueText) && !EnableCaseInsensitive)
                {
                    convertedPairs = null;
                    return false;
                }

                if (valueText == null)
                {
                    var list = namedValues.Keys.Where(key => string.Equals(kvp.Key, key, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (list.Count > 1)
                    {
                        throw new Exception();
                        //throw new ODataException(Strings.UriParserMetadata_MultipleMatchingKeysFound(kvp.Key));
                    }
                    else if (list.Count == 0)
                    {
                        convertedPairs = null;
                        return false;
                    }

                    valueText = namedValues[list.Single()];
                }

                object convertedValue = convertFunc(kvp.Value.Type, valueText);
                if (convertedValue == null)
                {
                    convertedPairs = null;
                    return false;
                }

                pairs[kvp.Key] = convertedValue;
            }

            convertedPairs = pairs;
            return true;
        }
    }


    /// <summary>
    /// Adds functionality to parse string arguments e.g. "1/2", "1''/2".
    /// </summary>
    public class StringParameterHandler : DefaultODataPathHandler
    {
        /// <summary>
        /// Parses the specified OData path by removing special string characters from the equation.
        /// </summary>
        /// <param name="serviceRoot"></param>
        /// <param name="odataPath"></param>
        /// <param name="requestContainer"></param>
        /// <returns></returns>
        public override Microsoft.AspNet.OData.Routing.ODataPath Parse(string serviceRoot, string odataPath, IServiceProvider requestContainer)
        {
            // This is why string primary keys are a bad idea.
            // odata/Resource/PrimaryKeyWith/InIt
            // odata/Resource('PrimaryKeyWith'InIt')

            return base.Parse(serviceRoot, EscapePath(odataPath), requestContainer);
        }

        /// <summary>
        /// Escapes special characters.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string EscapePath(string path)
        {
            var escapedPathBuilder = new StringBuilder();
            var isString = false;
            for (var i = 0; i < path.Length; i++)
            {
                switch (path[i])
                {
                    case '\'':
                        if (isString)
                        {
                            // If a quote is in a string argument a check must be performed if this quote ends the string argument.
                            var untilEnd = path.Substring(i);

                            if (Regex.Match(untilEnd, @"^' *,([0-9A-Za-z _]+=)?? *'", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else if (Regex.Match(untilEnd, @"^' *\) *\/[0-9A-Za-z _]+ *$", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else if (Regex.Match(untilEnd, @"^' *\) *\/[0-9A-Za-z _]+\(", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else if (Regex.Match(untilEnd, @"^' *\) *$", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else if (Regex.Match(untilEnd, @"^' *\) *\?\$(count|expand|filter|format|inlinecount|orderby|select|skip|top) *=", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else if (Regex.Match(untilEnd, @"^' *\) *\/\$(count|value) *$", RegexOptions.Compiled).Success)
                            {
                                escapedPathBuilder.Append(path[i]);
                                isString = false;
                            }
                            else
                            {
                                // If this is a string argument we must escape a quote.
                                escapedPathBuilder.Append("''"); //Uri.HexEscape('\'') doesn't work.
                            }
                        }
                        else
                        {
                            // A single quote signifies the start of a string argument.
                            // If the part before the quote was not a string argument, then starting after this quote it is a string argument.
                            escapedPathBuilder.Append(path[i]);
                            isString = true;
                        }
                        break;
                    case '/':
                        if (isString)
                        {
                            // If this is a string argument we must escape a slash.
                            escapedPathBuilder.Append(Uri.HexEscape('/'));
                        }
                        else
                        {
                            escapedPathBuilder.Append('/');
                        }
                        break;
                    default:
                        escapedPathBuilder.Append(path[i]);
                        break;
                }
            }

            return escapedPathBuilder.ToString();
        }
    }
}
