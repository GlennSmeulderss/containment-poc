using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.Models
{

    public class SiteFunction
    {
        /// <summary>
        /// The unique identifier of this entity.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="CreatedBy" />.
        /// </summary>
        public Guid? CreatedById { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="Site" />.
        /// </summary>
        public Guid SiteId { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="SiteFunctionType" />.
        /// </summary>
        public Guid SiteFunctionTypeId { get; set; }

        /// <summary>
        /// The date and time this <see cref="SiteFunction" /> was created.
        /// The <see cref="CreatedBy" /> is the <see cref="Identity" /> who created this <see cref="SiteFunction" />.
        /// </summary>
        [Column(TypeName = "datetime2(2)")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Specifies whether this <see cref="SiteFunction" /> is archived.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Specifies whether this <see cref="SiteFunction" /> is primary.
        /// </summary>
        public bool IsPrimary { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="ModifiedBy" />.
        /// </summary>
        public Guid? ModifiedById { get; set; }

        /// <summary>
        /// The automatically generated version number assigned to this entity.
        /// </summary>
        /// <remarks>long instead of ulong for OData $filter compatibility</remarks>
        [Timestamp]
        public long RowVersion { get; set; }


        /// <summary>
        /// The date and time this <see cref="SiteFunction" /> was last modified.
        /// The <see cref="ModifiedBy" /> is the <see cref="Identity" /> who last modified this <see cref="SiteFunction" />.
        /// </summary>
        [Column("HistoryStateStartedOn", TypeName = "datetime2(2)")]
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// The <see cref="Site" /> That belongs to this <see cref="SiteFunction" />.
        /// </summary>
        public virtual Site Site { get; set; }
    }
}
