using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.OData.Builder;

namespace BookStore.Models
{
    public class Site
    {
        /// <summary>
        /// The unique identifier of this entity.
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="CreatedBy" />.
        /// </summary>
        public Guid? CreatedById { get; set; }

        // /// <summary>
        // /// The name of this <see cref="Site" />.
        // /// </summary>
        // [Required]
        // public MultilingualString Name { get; set; }

        /// <summary>
        /// The email address of this <see cref="Site" />.
        /// </summary>
        [Column(TypeName = "varchar(100)")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// The phone number of this <see cref="Site" />.
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The fax number of this <see cref="Site" />.
        /// </summary>
        [Column(TypeName = "varchar(50)")]
        public string FaxNumber { get; set; }

        /// <summary>
        /// The date and time this <see cref="Site" /> was created.
        /// The <see cref="CreatedBy" /> is the <see cref="Identity" /> who created this <see cref="Site" />.
        /// </summary>
        [Column(TypeName = "datetime2(2)")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Specifies whether this <see cref="Site" /> is archived.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// The unique identifier of the <see cref="ModifiedBy" />.
        /// </summary>
        public Guid? ModifiedById { get; set; }

        /// <summary>
        /// The automatically generated version number assigned to this entity.
        /// </summary>
        /// <remarks>long instead of ulong for OData $filter compatibility</remarks>
        [Timestamp]
        public long RowVersion { get; set; }

        /// <summary>
        /// The date and time this <see cref="Site" /> was last modified.
        /// The <see cref="ModifiedBy" /> is the <see cref="Identity" /> who last modified this <see cref="Site" />.
        /// </summary>
        [Column("HistoryStateStartedOn", TypeName = "datetime2(2)")]
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// Collection of <see cref="SiteFunction" /> linked to this <see cref="Site" />.
        /// </summary>
        public virtual ICollection<SiteFunction> SiteFunctions { get; set; }

    }
}
