using System;
using System.Collections.Generic;
using System.Linq;
using BookStore.Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.Controllers
{
    [ApiVersion("1.0")]
    public class SitesController : ODataController
    {
        // PUT ~/Sites(42bf21b3-744d-4ab7-98d9-5b9b052f0a79)/SiteFunctions(38a413dc-8f2d-4b1c-a1c7-eae459bbef34)         
        [ODataRoute("Sites({siteId})/SiteFunctions({siteFunctionId})")]
        [HttpPut]
        public IActionResult PutToSiteFunction(Guid siteId, Guid siteFunctionId, [FromBody]SiteFunction siteFunction)
        {
            return Ok(siteFunction);
        }

        [ODataRoute("Sites({siteId})/SiteFunctions")]
        [HttpPost]
        public IActionResult PostToSiteFunction(Guid siteId, [FromBody]SiteFunction siteFunction)
        {
            return Ok(siteFunction);
        }
    }
}
